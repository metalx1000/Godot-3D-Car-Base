#Godot-3D-Car-Base

Copyright Kris Occhipinti 2021-09-20

(https://filmsbykris.com)

License GPLv3

This work is based on the tutorial from 

(https://github.com/kidscancode/3d_car_tutorial)

Original work License:

(https://github.com/kidscancode/3d_car_tutorial/blob/main/LICENSE)
